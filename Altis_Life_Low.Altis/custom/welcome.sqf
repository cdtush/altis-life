/*
	File: welcomecredits.sqf
	Author: HellsGateGaming.com - Edited for soligaming.com
	Date: 14/12/2013
	Description:
	Creates an intro on the bottom-right hand corner of the screen.
*/

_onScreenTime = 4;

sleep 0; //Wait in seconds before the credits start after player is in-game

playsound "intro";	

_role1 = "Good day good sir!";
_role1names = ["Welcome to", "SOLI Gaming"];
_role2 = "Chat with us on Teamspeak!";
_role2names = ["ts.soligaming.com"];
_role3 = "Developed from an idea by TAW_Tonic";
_role3names = ["by Many Like minded people"];
_role4 = "Please be sure to check the map";
_role4names ["Also Keep in mind that the Rebel License is available at the Rebel Head-Quarters"]
_role5 = "Rules :";
_role5names = ["No Theft In a Green Zone What So ever<br/>NO TROLLING<br/>NO VOICE OVER SIDE<br/>REBEL SPAWNS/OUTPOSTS ARE NO KILL ZONES/ NO CRIME ZONES"];
_role6 = "Rules :";
_role6names = ["NO RDM / VDM PERIOD<br/>Thank you!"];
_role7 = "Rules :";
_role7names = ["Please Keep in mind,  that The police have the right to take your guns if they are visible."];
_role8 = "Enjoy your stay";
_role8names = ["Have fun and follow the rules<br/>Like The Server?<br/>Please support us by Telling your friends!<br/>"];
{
sleep 2;
_memberFunction = _x select 0;
_memberNames = _x select 1;
_finalText = format ["<t size='0.40' color='#0033CC' align='right'>%1<br /></t>", _memberFunction];
_finalText = _finalText + "<t size='0.70' color='#FFFFFF' align='right'>";
{_finalText = _finalText + format ["%1<br />", _x]} forEach _memberNames;
_finalText = _finalText + "</t>";
_onScreenTime + (((count _memberNames) - 1) * 0.5);
[
_finalText,
[safezoneX + safezoneW - 0.8,0.50], //DEFAULT: 0.5,0.35
[safezoneY + safezoneH - 0.8,0.7], //DEFAULT: 0.8,0.7
_onScreenTime,
0.5
] spawn BIS_fnc_dynamicText;
sleep (_onScreenTime);
} forEach [
//The list below should have exactly the same amount of roles as the list above
[_role1, _role1names],
[_role2, _role2names],
[_role3, _role3names],
[_role4, _role4names],
[_role5, _role5names],
[_role6, _role6names],
[_role7, _role7names],
[_role8, _role8names]
];