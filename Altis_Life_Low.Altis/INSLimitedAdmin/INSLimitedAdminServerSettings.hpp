//MAIN SETTINGS INSLimitedAdmin
 
_pboclassesdetect = true; //Scan player PBO files ("true" is on, "false" is off, set to true by default)
_pboclasses = ["stra_debug2","DevCon","mf_sdc","tao_a3_debugger","KRON_SupportCall"]; //Classnames from addons you don't wan't on your server
 
 
_bannedvarsdetect = false; //Detect player using Banned variable names ("true" is on, "false" is off, set to flase by default)
_bannedvars = ["life_coin","life_funds"]; //Banned variable names (e.g. an array called: playerteleport)
 
 
_speedhackdetect = true; //Detect player speed hacking ("true" is on, "false" is off, set to true by default)
_maxspeed = 400; //Non-unit vehicle max speed on your server (Variable for detecting speed-hacking, set to 400 by default)
 
 
_bannedwepsdetect = true; //Detect player using Banned weapon ("true" is on, "false" is off, set to flase by default)
_bannedweps = ["srifle_LRR_F","srifle_LRR_SOS_F","srifle_LRR_LRPS_F","srifle_LRR_camo_F","optic_Nightstalker","optic_NVS","optic_tws","optic_tws_mg","LMG_Mk200_F","LMG_Mk200_MRCO_F","LMG_Mk200_pointer_F","LMG_Zafir_F","LMG_Zafir_pointer_F","LMG_Zafir_ARCO_F","srifle_DMR_05_hex_F","srifle_DMR_05_tan_f","srifle_DMR_05_ACO_F","srifle_DMR_05_MRCO_F","srifle_DMR_05_SOS_F","srifle_DMR_05_DMS_F","srifle_DMR_05_KHS_LP_F","srifle_DMR_05_DMS_snds_F","srifle_DMR_05_ARCO_F","MMG_01_hex_F","MMG_01_tan_F","MMG_01_hex_ARCO_LP_F","MMG_02_camo_F","MMG_02_black_F","MMG_02_sand_F","MMG_02_sand_RCO_LP_F","MMG_02_black_RCO_BI_F"]; //Weapons you can't obtain legitimately in your mission
 
 
_bannedvclsdetect = true; //Detect player using Banned vehicle ("true" is on, "false" is off, set to flase by default)
_bannedvcls = ["B_Hunter_F","B_Heli_Attack_01_F","O_Heli_Attack_02_F","O_Heli_Attack_02_black_F","B_APC_Wheeled_01_cannon_F","B_APC_Tracked_01_rcws_F","B_APC_Tracked_01_CRV_F","B_APC_Tracked_01_AA_F","B_MRAP_01_gmg_F",
"O_APC_Tracked_02_cannon_F","O_APC_Tracked_02_AA_F","O_MBT_02_cannon_F","O_MRAP_02_gmg_F","O_APC_Wheeled_02_rcws_F","I_APC_tracked_03_cannon_F","I_MBT_03_cannon_F","I_MRAP_03_gmg_F","I_APC_Wheeled_03_cannon_F"]; //Vehicles you can't obtain legitimately in your mission
 
 
_teleportdetect = false; //Detect player teleporting ("true" is on, "false" is off, set to false by default, MAY CAUSE LAGG)
 
 
_banmessage = "Our antihack has detected that you may have done some thing that you shouldn't have, Please exit ARMA 3, Disable any addons or mods, and Rejoin our server."; //Message Displayed To Hacker When Detected
 
//Do NOT edit below this point
 
INSLimitedAdminServerVariables = [_pboclassesdetect,_pboclasses,_bannedvarsdetect,_bannedvars,_speedhackdetect,_maxspeed,_bannedwepsdetect,_bannedweps,_bannedvclsdetect,_bannedvcls,_teleportdetect,_banmessage];