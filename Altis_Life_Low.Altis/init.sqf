enableSaving [false, false];

X_Server = false;
X_Client = false;
X_JIP = false;
StartProgress = false;

if(!isDedicated) then { X_Client = true;};
enableSaving[false,false];

life_versionInfo = "Altis Life RPG v3.1.4.8";
[] execVM "briefing.sqf"; //Load Briefing
[] execVM "noside.sqf";
[] execVM "KRON_Strings.sqf";

//Load addon scripts
//[] execVM "scripts\fieldRandom.sqf";
//[] execVM "INSLimitedAdmin\initAH.sqf";
//execVM "scripts\earplug\earplugInit.sqf";
{_x setMarkerAlphaLocal 0} forEach ["mrkRed","mrkRed_1","mrkRed_1_1","mrkRed_1_3","mrkGreen"];
StartProgress = true;

"BIS_fnc_MP_packet" addPublicVariableEventHandler {_this call life_fnc_MPexec};