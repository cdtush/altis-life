/*
	File: fn_varToStr.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Takes the long-name (variable) and returns a display name for our
	virtual item.
*/
private["_var"];
_var = [_this,0,"",[""]] call BIS_fnc_param;
if(_var == "") exitWith {""};

switch (_var) do
{
	//Virtual Inventory Items
	case "life_inv_oilu": {(localize "STR_Item_OilU")};
	case "life_inv_oilp": {(localize "STR_Item_OilP")};
	case "life_inv_heroinu": {(localize "STR_Item_HeroinU")};
	case "life_inv_heroinp": {(localize "STR_Item_HeroinP")};
	case "life_inv_cannabis": {(localize "STR_Item_Cannabis")};
	case "life_inv_marijuana": {(localize "STR_Item_Marijuana")};
	case "life_inv_apple": {(localize "STR_Item_Apple")};
	case "life_inv_rabbit": {(localize "STR_Item_RabbitMeat")};
	case "life_inv_salema": {(localize "STR_Item_SalemaMeat")};
	case "life_inv_ornate": {(localize "STR_Item_OrnateMeat")};
	case "life_inv_mackerel": {(localize "STR_Item_MackerelMeat")};
	case "life_inv_tuna": {(localize "STR_Item_TunaMeat")};
	case "life_inv_mullet": {(localize "STR_Item_MulletMeat")};
	case "life_inv_catshark": {(localize "STR_Item_CatSharkMeat")};
	case "life_inv_turtle": {(localize "STR_Item_TurtleMeat")};
	case "life_inv_fishingpoles": {(localize "STR_Item_FishingPole")};
	case "life_inv_water": {(localize "STR_Item_WaterBottle")};
	case "life_inv_coffee": {(localize "STR_Item_Coffee")};
	case "life_inv_turtlesoup": {(localize "STR_Item_TurtleSoup")};
	case "life_inv_donuts": {(localize "STR_Item_Donuts")};
	case "life_inv_fuelE": {(localize "STR_Item_FuelE")};
	case "life_inv_fuelF": {(localize "STR_Item_FuelF")};
	case "life_inv_pickaxe": {(localize "STR_Item_Pickaxe")};
	case "life_inv_copperore": {(localize "STR_Item_CopperOre")};
	case "life_inv_ironore": {(localize "STR_Item_IronOre")};
	case "life_inv_ironr": {(localize "STR_Item_IronIngot")};
	case "life_inv_copperr": {(localize "STR_Item_CopperIngot")};
	case "life_inv_sand": {(localize "STR_Item_Sand")};
	case "life_inv_salt": {(localize "STR_Item_Salt")};
	case "life_inv_saltr": {(localize "STR_Item_SaltR")};
	case "life_inv_glass": {(localize "STR_Item_Glass")};
	case "life_inv_diamond": {(localize "STR_Item_DiamondU")};
	case "life_inv_diamondr": {(localize "STR_Item_DiamondC")};
	case "life_inv_tbacon": {(localize "STR_Item_TBacon")};
	case "life_inv_redgull": {(localize "STR_Item_RedGull")};
	case "life_inv_lockpick": {(localize "STR_Item_Lockpick")};
	case "life_inv_peach": {(localize "STR_Item_Peach")};
	case "life_inv_coke": {(localize "STR_Item_CocaineU")};
	case "life_inv_cokep": {(localize "STR_Item_CocaineP")};
	case "life_inv_spikeStrip": {(localize "STR_Item_SpikeStrip")};
	case "life_inv_rock": {(localize "STR_Item_Rock")};
	case "life_inv_cement": {(localize "STR_Item_CementBag")};
	case "life_inv_goldbar": {(localize "STR_Item_GoldBar")};
	case "life_inv_goldcoin": {(localize "STR_Item_GoldCoin")};
	case "life_inv_blastingcharge": {(localize "STR_Item_BCharge")};
	case "life_inv_boltcutter": {(localize "STR_Item_BCutter")};
	case "life_inv_defusekit": {(localize "STR_Item_DefuseKit")};
	case "life_inv_storagesmall": {(localize "STR_Item_StorageBS")};
	case "life_inv_storagebig": {(localize "STR_Item_StorageBL")};
	case "life_inv_storagehuge": {(localize "STR_Item_StorageXL")};
	case "life_inv_burger": {(localize "STR_Item_Burger")};
	case "life_inv_hotdog": {(localize "STR_Item_HotDog")};
	case "life_inv_crayfish": {(localize "STR_Item_CrayFish")};
	case "life_inv_mash": {(localize "STR_Item_Mash")};
	case "life_inv_corn": {(localize "STR_Item_Corn")};
	case "life_inv_rye": {(localize "STR_Item_Rye")};
	case "life_inv_hops": {(localize "STR_Item_Hops")};
	case "life_inv_whiskey": {(localize "STR_Item_DistilledWhiskey")};
	case "life_inv_beerp": {(localize "STR_Item_FermentedBeer")};
	case "life_inv_bottles": {(localize "STR_Item_GlassBottles")};
	case "life_inv_cornmeal": {(localize "STR_Item_CornmealGrains")};
	case "life_inv_bottledwhiskey": {(localize "STR_Item_BottledWhiskey")};
	case "life_inv_bottledbeer": {(localize "STR_Item_BottledBeer")};
	case "life_inv_bottledshine": {(localize "STR_Item_BottledMoonshine")};
	case "life_inv_moonshine": {(localize "STR_Item_DistilledMoonshine")};
	case "life_inv_kidney": {(localize "STR_Item_Kidney")};
	case "life_inv_scalpel": {(localize "STR_Item_Scalpel")};
	case "life_inv_lung": {(localize "STR_Item_Lung")};
	case "life_inv_spleen": {(localize "STR_Item_Spleen")};
	case "life_inv_underwatercharge": {(localize "STR_Item_UCharge")};
	case "life_inv_goldbarp": {(localize "STR_Item_GoldScrap")};
	case "life_inv_leg": {(localize "STR_Item_leg")};
	case "life_inv_testicles": {(localize "STR_Item_testicles")};
	case "life_inv_eyes": {(localize "STR_Item_eyes")};
	case "life_inv_gpstracker": {(localize "STR_Item_gpstracker")};
	case "life_inv_bodypart": {(localize "STR_Item_bodypart")};
	case "life_inv_draincleaner": {localize "STR_Item_DrainCleaner"};
	case "life_inv_methadone": {localize "STR_Item_Methadone"};
	case "life_inv_meth": {localize "STR_Item_Meth"};
	case "life_inv_pinkmeth": {localize "STR_Item_PinkMeth"};
	case "life_inv_handcuffs": {(localize "STR_Item_Handcuffs")};
    case "life_inv_handcuffkeys": {(localize "STR_Item_Handcuffkeys")};
	case "life_inv_wheat": {(localize "STR_Item_Wheat")};
	
	//License Block
	case "license_civ_driver": {(localize "STR_License_Driver")};
	case "license_civ_air": {(localize "STR_License_Pilot")};
	case "license_civ_heroin": {(localize "STR_License_Heroin")};
	case "license_civ_oil": {(localize "STR_License_Oil")};
	case "license_civ_dive": {(localize "STR_License_Diving")};
	case "license_civ_boat": {(localize "STR_License_Boat")};
	case "license_civ_gun": {(localize "STR_License_Firearm")};
	case "license_cop_air": {(localize "STR_License_Pilot")};
	case "license_cop_swat": {(localize "STR_License_Swat")};
	case "license_cop_cg": {(localize "STR_License_CG")};
	case "license_civ_rebel": {(localize "STR_License_Rebel")};
	case "license_civ_truck": {(localize "STR_License_Truck")};
	case "license_civ_diamond": {(localize "STR_License_Diamond")};
	case "license_civ_copper": {(localize "STR_License_Copper")};
	case "license_civ_iron": {(localize "STR_License_Iron")};
	case "license_civ_sand": {(localize "STR_License_Sand")};
	case "license_civ_salt": {(localize "STR_License_Salt")};
	case "license_civ_coke": {(localize "STR_License_Cocaine")};
	case "license_civ_marijuana": {(localize "STR_License_Marijuana")};
	case "license_civ_cement": {(localize "STR_License_Cement")};
	case "license_med_air": {(localize "STR_License_Pilot")};
	case "license_civ_home": {(localize "STR_License_Home")};
	case "license_civ_stiller": {(localize "STR_License_DistillersLicense")};
	case "license_civ_liquor": {(localize "STR_License_LiquorLicense")};
	case "license_civ_bottler": {(localize "STR_License_BottlingLicense")};
	case "license_civ_advrebel": {(localize "STR_License_AdvRebel")};
	case "license_civ_tow": {(localize "STR_License_Tow")};
	case "license_civ_katiba": {(localize "STR_Legel_Katiba")};
	case "license_civ_taxi": {(localize "STR_License_Taxi")};
	case "license_civ_petrol": {(localize "STR_License_Petrol")};
	case "license_civ_admin": {(localize "STR_License_Admin")};
	case "license_civ_meth": {(localize "STR_License_Meth")};
	case "license_civ_pinkmeth": {(localize "STR_License_PinkMeth")};
	case "license_civ_mech": {(localize "STR_License_Mech")};
	case "license_civ_jreb": {(localize "STR_License_JReb")};
	case "license_civ_news": {(localize "STR_License_News")};
};
