#include <macro.h>
/*
	File: fn_seizeCfg.sqf
	Author: Tabakhase
	Slightly adapted by Daniel Larusso (Keep Calm and Roleplay)
	
	Description:
	Master configuration file for seize player everything.
	
	Parameters:
	0 = All
	1 = Weapons
	2 = Uniform
	3 = Backpack
	4 = Vest
	5 = Items
	
	ToDo:
	Adding list 0,2,3,4&5
*/
private["_mode"];
_mode = [_this,0,objNull,[""]] call BIS_fnc_param;

switch(_mode) do
{
	case 1: {
		[
			// Weapons
			"srifle_EBR_F",
			"srifle_GM6_F",
			"srifle_LRR_F",
			"LMG_Mk200_F",
			"hgun_P07_F",
			"hgun_Rook40_F",
			"hgun_ACPC2_F",
			"hgun_Pistol_heavy_01_F",
			"hgun_Pistol_heavy_01_snds_F",
			"hgun_Pistol_heavy_02_F",
			"hgun_PDW2000_F",
			"hgun_Pistol_Signal_F",
			"arifle_Katiba_F",
			"arifle_Katiba_C_F",
			"arifle_Katiba_GL_F",
			"arifle_MXC_F",
			"arifle_MXC_Black_F",
			"arifle_MX_F",
			"arifle_MX_Black_F",
			"arifle_MX_GL_F",
			"arifle_MX_GL_Black_F",
			"arifle_MX_SW_F",
			"arifle_MX_SW_Black_F",
			"arifle_MXM_F",
			"arifle_MXM_Black_F",
			"arifle_SDAR_F",
			"arifle_TRG21_F",
			"arifle_TRG20_F",
			"arifle_TRG21_GL_F",
			"arifle_Mk20_F",
			"arifle_Mk20_plain_F",
			"arifle_Mk20C_F",
			"arifle_Mk20C_plain_F",
			"arifle_Mk20_GL_F",
			"arifle_Mk20_GL_plain_F",
			"launch_B_Titan_F",
			"launch_I_Titan_F",
			"launch_O_Titan_F",
			"launch_B_Titan_short_F",
			"launch_I_Titan_short_F",
			"launch_O_Titan_short_F",
			"LMG_Zafir_F",
			"SMG_01_F",
			"SMG_02_F",
			"srifle_DMR_01_F",
			"srifle_DMR_06_camo_F",
			"srifle_DMR_04_Tan_F"
		];
	};
};