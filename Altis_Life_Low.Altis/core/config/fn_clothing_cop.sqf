#include <macro.h>
/*
	File: fn_clothing_cop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master config file for Cop clothing store.
*/
private["_filter","_ret"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Altis Police Department Shop"];

_ret = [];
switch (_filter) do
{
	//Uniforms
	case 0:
	{
		_ret set[count _ret,["U_Rangemaster","Police Uniform",25]];
		if(__GETC__(life_coplevel) == 5) then
		{
			_ret set[count _ret,["U_B_HeliPilotCoveralls","Pilot Uniform",550]];
		};
		
		if(__GETC__(life_coplevel) > 6) then
		{
		_ret set[count _ret,["U_B_CombatUniform_mcam_worn","Swat Uniform",550]];
		};
		
	[] call life_fnc_copUniform;
	};
	
	//Hats
	case 1:
	{
		if(__GETC__(life_coplevel) > 0) then
		{
			_ret set[count _ret,["H_Cap_police","Police Hat",20]];
		};
		
		if(__GETC__(life_coplevel) > 1) then
		{
			[] call life_fnc_copUniform;
			_ret set[count _ret,["H_Cap_blk","Black Hat",20]];
			_ret set[count _ret,["H_Beret_blk_POLICE","Police Beret",20]];
			_ret set[count _ret,["H_Watchcap_blk","Police Beanie",20]];
		};
		if(__GETC__(life_coplevel) == 5) then
		{
			_ret set[count _ret,["H_PilotHelmetHeli_B",nil,200]];
		};
		if(__GETC__(life_coplevel) > 6 ) then
		{
		_ret set[count _ret,["H_HelmetB_black","Swat Helment",100]];
		_ret set[count _ret,["H_Beret_Colonel",nil,200]];
		_ret set[count _ret,["H_Booniehat_mcamo",nil,120]];
		};
	};
	
	//Glasses
	case 2:
	{
		_ret = 
		[
			["G_Shades_Black",nil,25],
			
			["G_Shades_Blue",nil,20],
			["G_Sport_Blackred",nil,20],
			["G_Sport_Checkered",nil,20],
			["G_Sport_Blackyellow",nil,20],
			["G_Sport_BlackWhite",nil,20],
			["G_Aviator",nil,75],
			["G_Squares",nil,10],
			["G_Lowprofile",nil,30],
			["G_Combat",nil,55]
		];
		
		if(__GETC__(life_coplevel) > 6 ) then
		{
		_ret set[count _ret,["G_Balaclava_combat","Swat Balaclava",20]];
		_ret set[count _ret,["G_Bandanna_aviator","Swat Bandana",20]];
		};
		[] call life_fnc_copUniform;
	};
	
	//Vest
	case 3:
	{
		_ret set[count _ret,["V_Rangemaster_belt",nil,800]];
		if(__GETC__(life_coplevel) > 1) then
		{
			_ret set[count _ret,["V_TacVest_blk_POLICE","Police Vest",1500]];
		};
		
		if(__GETC__(life_coplevel) > 4) then
		{
			_ret set[count _ret,["V_TacVestIR_blk","Black Vest",1500]];
		};
		
		if(__GETC__(life_coplevel) > 6) then
		{
			_ret set[count _ret,["V_PlateCarrier1_blk","Swat Vest",1500]];
		};
	[] call life_fnc_copUniform;
	};
	
	
	//Backpacks
	case 4:
	{
		_ret =
		[
			["B_Kitbag_cbr",nil,800],
			["B_FieldPack_cbr",nil,500],
			["B_AssaultPack_cbr",nil,700],
			["B_Bergen_sgg",nil,2500],
			["B_Carryall_cbr",nil,3500],
			["B_AssaultPack_blk",nil,700]
		];
	[] call life_fnc_copUniform;
	};
};

_ret;