#include <macro.h>
/*
	File:
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration list / array for buyable vehicles & prices and their shop.
*/
private["_shop","_return"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {[]};
_return = [];
switch (_shop) do
{
	case "kart_shop":
	{
		_return = [
			["C_Kart_01_Blu_F",15000],
			["C_Kart_01_Fuel_F",15000],
			["C_Kart_01_Red_F",15000],
			["C_Kart_01_Vrana_F",15000]
		];
	};
	case "med_shop":
	{
		_return = [
			["C_Offroad_01_F",15000],
			["C_Offroad_01_repair_F",15000],
			["C_SUV_01_F",30000]
		];
	};
	
	case "med_air_hs": {
		_return = [
			["B_Heli_Light_01_F",60000],
			["O_Heli_Light_02_unarmed_F",85000]
		];
	};
	
	case "civ_car":
	{
		_return = 
		[
			["B_Quadbike_01_F",3500],
			["C_Hatchback_01_F",20500],
			["C_Offroad_01_F",32500],
			["C_SUV_01_F",45000],
			["C_Hatchback_01_sport_F",300000],
			["C_Van_01_transport_F",50000],
			["O_G_Van_01_fuel_F",55000]

		];
		
		if(license_civ_katiba) then	
		{
			_return pushBack
			["I_MRAP_03_F",150000];
		};
	};
	
	case "civ_taxi":
	{	
		if(license_civ_taxi) then	
		{
			_return pushBack
			["C_SUV_01_F",39000]
		};
	};
	
	case "civ_truck":
	{
		_return =
		[
			["C_Van_01_box_F",80000],
			["I_Truck_02_transport_F",95000],
			["I_Truck_02_covered_F",125000],
			["B_Truck_01_transport_F",300000],
			["O_Truck_03_transport_F",350000],
			["O_Truck_03_covered_F",330000],
			["B_Truck_01_mover_F",400000],
			["B_Truck_01_box_F",500000],
			["O_Truck_03_device_F",500000]
		];	
	};
	
	case "reb_car":
	{
		_return =
		[
			["B_Quadbike_01_F",3500],
			["B_G_Offroad_01_F",15000],
			["O_MRAP_02_F",300000],
			["B_Heli_Light_01_F",400000],
			["C_Heli_Light_01_civil_F",400000]			
		];
		
		if(license_civ_rebel) then
		{
			_return pushBack
			["B_G_Offroad_01_armed_F",1000000];
			_return pushBack
			["O_Heli_Light_02_unarmed_F",1500000];
		};
	};
	
	case "cop_car":
	{
		_return =
		[
			["C_Offroad_01_F",8000]
		];
		
		if(__GETC__(life_coplevel) >= 1) then
		{
			_return pushBack
			["C_Hatchback_01_F",14000];
			_return pushBack
			["C_SUV_01_F",25000];
		};
		
		if(__GETC__(life_coplevel) >= 5) then
		{
			_return pushBack
			["B_MRAP_01_F",45000];
		};
	};
	
	case "civ_air":
	{
		_return =
		[
			["B_Heli_Light_01_F",253000],
			["O_Heli_Light_02_unarmed_F",750000],
			["I_Heli_Transport_02_F",1250000],
			["C_Heli_Light_01_civil_F",250000],
			["O_Heli_Transport_04_fuel_F",1750000],
			["O_Heli_Transport_04_covered_F",1750000],
			["O_Heli_Transport_04_box_F",1750000],
			["B_Heli_Transport_03_unarmed_F",2000000],
			["O_Heli_Transport_04_F",2000000]
		];
	};
	
	case "cop_air":
	{
		if(__GETC__(life_coplevel) >= 2) then
		{
			_return pushBack
			["B_Heli_Light_01_F",75000];
			_return pushBack
			["C_Heli_Light_01_civil_F",75000];
		};	
		if(__GETC__(life_coplevel) >= 5) then
		{
			_return pushBack
			["I_Heli_light_03_unarmed_F",200000];
			_return pushBack
			["B_Heli_Transport_01_F",400000];
			_return pushBack
			["B_Heli_Transport_03_unarmed_F",600000];
		};
	};
	
	case "cop_airhq":
		{
		if(__GETC__(life_coplevel) >= 5) then
		{
			_return pushBack
			["B_Heli_Light_01_F",75000];
			_return pushBack
			["C_Heli_Light_01_civil_F",75000];
			_return pushBack
			["I_Heli_light_03_unarmed_F",200000];
			_return pushBack
			["B_Heli_Transport_01_F",400000];
			_return pushBack
			["B_Heli_Transport_03_unarmed_F",600000];
		};
		
		if(__GETC__(life_coplevel) > 6) then
		{
			_return pushBack
			["B_MRAP_01_hmg_F",750000];
		};
	};
	
	case "news_crew":
	{
		if(license_civ_news) then	
		{
			_return pushBack
			["I_Heli_light_03_unarmed_F",600000];
		};
	};
	
	case "civ_ship":
	{
		_return =
		[
			["C_Rubberboat",8000],
			["O_SDV_01_F",550000],
			["C_Boat_Civil_01_F",29000]
		];
	};

	case "cop_ship":
	{
		_return =
		[
			["B_Boat_Transport_01_F",3000],
			["C_Boat_Civil_01_police_F",20000],
			["B_SDV_01_F",100000]
		];
	};
};

_return;