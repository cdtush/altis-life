/*
	File: fn_virt_shops.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Config for virtual shops.
*/
private["_shop"];
_shop = _this select 0;

switch (_shop) do
{
	case "market": {["Altis Market",["water","bottles","rabbit","apple","redgull","tbacon","pickaxe","fuelF","peach","gpstracker"]]};
	case "rebel": {["Rebel Market",["water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","boltcutter","blastingcharge"]]};
	case "arebel": {["Cuffs n Stuff",["handcuffs","handcuffkeys"]]};
	case "gang": {["Gang Market", ["water","rabbit","apple","redgull","tbacon","lockpick","pickaxe","fuelF","peach","blastingcharge","boltcutter"]]};
	case "wongs": {["Wong's Food Cart",["turtlesoup","turtle"]]};
	case "coffee": {["Stratis Coffee Club",["coffee","donuts"]]};
	case "heroin": {["Drug Dealer",["cocainep","heroinp","marijuana","meth","pinkmeth"]]};
	case "oil": {["Oil Trader",["oilp","pickaxe","fuelF"]]};
	case "fishmarket": {["Altis Fish Market",["salema","ornate","mackerel","mullet","tuna","catshark","crayfish"]]};
	case "glass": {["Altis Glass Dealer",["glass"]]};
	case "iron": {["Altis Industrial Trader",["iron_r","copper_r"]]};
	case "diamond": {["Diamond Dealer",["diamond","diamondc"]]};
	case "salt": {["Salt Dealer",["salt_r"]]};
	case "cop": {["Cop Item Shop",["donuts","coffee","spikeStrip","water","rabbit","apple","redgull","fuelF","defusekit","gpstracker","handcuffs","handcuffkeys"]]};
	case "cement": {["Cement Dealer",["cement"]]};
	case "gold": {["Gold Buyer",["goldbar","goldcoin","goldbarp","underwatercharge"]]};
	case "gasmart": {["gasmart",["water","redgull","tbacon","bottledbeer","fuelF"]]};
	case "redburger": {["Red-Burger",["burger","hotdog","bottledbeer"]]};
	case "bar": {["The Lounge",["bottledbeer","bottledwhiskey"]]};
	case "speakeasy": {["Speakeasy's",["bottledwhiskey","bottledshine","bottledbeer"]]};
	case "organ": {["Organ Dealer",["kidney","spleen","lung","testicles","eyes","leg"]]}; 
	case "medsup": {["Med Supplies",["scalpel","bodypart","methadone"]]};
	case "Prospector": {["Gold Ingot Dealer",["goldbarp","underwatercharge"]]};
	case "homestore": {["The Big Box Store",["storagesmall","gpstracker","storagebig","boltcutter","fuelF"]]};
	case "fmarket": {["Farmers Market",["apple","peach","wheat","corn"]]};
};