/*
	File: fn_licensePrice.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Returns the license price.
*/
private["_type"];
_type = [_this,0,"",[""]] call BIS_fnc_param;
if(_type == "") exitWith {-1};

switch (_type) do
{
	case "driver": {800}; //Drivers License cost
	case "boat": {2000}; //Boating license cost
	case "pilot": {30000}; //Pilot/air license cost
	case "gun": {12000}; //Firearm/gun license cost
	case "dive": {2500}; //Diving license cost
	case "oil": {10000}; //Oil processing license cost
	case "cair": {15000}; //Cop Pilot License cost
	case "swat": {35000}; //Swat License cost
	case "cg": {8000}; //Coast guard license cost
	case "heroin": {25000}; //Heroin processing license cost
	case "marijuana": {19500}; //Marijuana processing license cost
	case "medmarijuana": {1500}; //Medical Marijuana processing license cost
	case "gang": {25000}; //Gang license cost
	case "rebel": {275000}; //Rebel license cost
	case "jreb": {95000};
	case "truck": {20000}; //Truck license cost
	case "diamond": {35000};
	case "salt": {12000};
	case "cocaine": {30000};
	case "sand": {14500};
	case "iron": {9500};
	case "copper": {8000};
	case "cement": {6500};
	case "mair": {15000};
	case "home": {75000};
	case "stiller": {50000};
	case "liquor": {10000};
	case "bottler": {10000};
	case "mash": {10000};
	case "advrebel": {550000};
	case "tow": {125000};
	case "meth":{30000};
	case "pinkmeth":{65000};
	case "mech":{250000};
	case "news":{1000000};
};