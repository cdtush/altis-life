/*
	COP UNIFORM SCRIPT
	Author: coldgas (http://altis.newhopeoutfit.de)
	Created for www.altisliferpg.com
*/

	#define __GETC__(var) (call var)
	
	if ((uniform player) == "U_Competitor")  then {
		player setObjectTextureGlobal [0, "textures\medic_uniform.jpg"];
	};
	
// call this script in as many files as possible, especially init_cop, init_civ and all files related to the cop-shop.