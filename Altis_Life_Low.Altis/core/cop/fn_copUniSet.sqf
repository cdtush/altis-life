/*
	COP UNIFORM SCRIPT
	Author: coldgas (http://altis.newhopeoutfit.de)
	Created for www.altisliferpg.com
*/

	#define __GETC__(var) (call var)
	
	// Standard Cop Uniforms by coplevel. Create new 'if' for every coplevel you want to have a special uniform. (e.G. if(__GETC__(life_coplevel) == 1) then {... )

	if (__GETC__(life_coplevel) > 0)  then {
		player setObjectTextureGlobal [0, "textures\police_shirt.jpg"]; // this is for all cops whitelisted or public.
	};

	// Special Uniforms cops can buy in shops. Add new 'if' for every classname you want the skin to be overwritten. (warning, does NOT work with every class!)
	
	if ((playerSide == west) && (uniform player) == "U_B_CombatUniform_mcam_worn")  then {
		player setObjectTextureGlobal [0, "textures\swat_shirt.jpg"];
	};

	if ((uniform player) == "U_B_HeliPilotCoveralls")  then {
		player setObjectTextureGlobal [0, "textures\pilot_uniform.jpg"];
	};
	
	if ((uniform player) == "V_PlateCarrierGL_rgr")  then {
		player setObjectTextureGlobal [0, "textures\swat_leader_vest.jpg"];
	};
	
	if ((uniform player) == "V_PlateCarrier_Kerry")  then {
		player setObjectTextureGlobal [0, "textures\swat_vest.jpg"];
	};
	
	if ((uniform player) == "H_Helmet_Kerry")  then {
		player setObjectTextureGlobal [0, "textures\swat_helmet.jpg"];
	};
	
	if ((uniform player) == "V_PlateCarrier2_rgr")  then {
		player setObjectTextureGlobal [0, "textures\police_vest.jpg"];
	};
	
	if ((uniform player) == "U_Rangemaster")  then {
		player setObjectTextureGlobal [0, "textures\police_shirt.jpg"];
	};
	
	if ((uniform player) == "U_Competitor")  then {
		player setObjectTextureGlobal [0, "textures\medic_uniform.jpg"];
	};
	
// call this script in as many files as possible, especially init_cop, init_civ and all files related to the cop-shop.