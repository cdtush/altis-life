#include <macro.h>
/*

	Player clicked arrest/ok

*/

private ["_time"];

if(playerSide != west) exitWith {};
if(isNil "life_pInact_curTarget") exitWith {hint "Invalid destination."};

//Get minutes
_time = ctrlText 1400;


if(! ([_time] call TON_fnc_isnumber)) exitWith
{
	hint "You have to enter a number.";
};

_time = parseNumber _time; //requested number
_time = round _time;
#include <macro.h>
/*

	Player clicked arrest/ok

*/
private ["_time","_exit"];
_exit = false;
if(playerSide != west) exitWith {};
if(isNil "life_pInact_curTarget") exitWith {hint "Invalid destination."};
//Get minutes

_time = ctrlText 1400;
if(! ([_time] call TON_fnc_isnumber)) exitWith
{
	hint "You have to enter a number.";
};

_time = parseNumber _time; //requested number
_time = round _time;
/*
if(__GETC__(life_coplevel) = 1)  then {
	if(_time < 1 || _time > 10) exitWith { hint "You can only go to jail between 1-10 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 2)  then {
	if(_time < 1 || _time > 15) exitWith { hint "You can only go to jail between 1-15 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 3)  then {
	if(_time < 1 || _time > 20) exitWith { hint "You can only go to jail between 1-20 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 4)  then {
	if(_time < 1 || _time > 25) exitWith { hint "You can only go to jail between 1-25 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 5)  then {
	if(_time < 1 || _time > 30) exitWith { hint "You can only go to jail between 1-30 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 6)  then {
	if(_time < 1 || _time > 35) exitWith { hint "You can only go to jail between 1-35 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 7)  then {
	if(_time < 1 || _time > 40) exitWith { hint "You can only go to jail between 1-40 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 8)  then {
	if(_time < 1 || _time > 45) exitWith { hint "You can only go to jail between 1-45 minutes!"; _exit = true;};
} else {
if(__GETC__(life_coplevel) = 9)  then {
	if(_time < 1 || _time > 90) exitWith { hint "You can only go to jail between 1-90 minutes!"; _exit = true;};
} else {
	if(_time < 1 || _time > 5) exitWith { hint "You can only go to jail between 1-5 minutes!"; _exit = true;};
};
if(_exit) exitWith {};
*/
if(_time < 5 || _time > 20) exitWith { hint hint "You can only go to jail between 5-20 minutes!"; };
closeDialog 0; 
[life_pInact_curTarget, _time] call life_fnc_arrestAction;