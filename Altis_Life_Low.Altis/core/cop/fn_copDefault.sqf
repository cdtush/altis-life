#include <macro.h>
/*
File: fn_copDefault.sqf
Author: Bryan "Tonic" Boardwine
 
Description:
Default cop configuration.
*/
//Strip the player down
RemoveAllWeapons player;
{player removeMagazine _x;} foreach (magazines player);
removeUniform player;
removeVest player;
removeBackpack player;
removeGoggles player;
removeHeadGear player;
{
player unassignItem _x;
player removeItem _x;
} foreach (assignedItems player);
 
//Load player with default cop gear.

if(__GETC__(life_coplevel) == 0) then
{
player addUniform "U_Rangemaster";
player addVest "V_Rangemaster_belt";
player addWeapon "hgun_P07_snds_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};

if(__GETC__(life_coplevel) == 1) then
{
player addUniform "U_Rangemaster";
player addVest "V_TacVest_blk_POLICE";
player addWeapon "hgun_P07_snds_F";
player addWeapon "arifle_SDAR_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};
 
if(__GETC__(life_coplevel) == 2) then
{
player addUniform "U_Rangemaster";
player addVest "V_TacVestIR_blk";
player addWeapon "hgun_P07_snds_F";
player addWeapon "SMG_02_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};
 
if(__GETC__(life_coplevel) == 3) then
{
player addUniform "U_Rangemaster";
player addVest "V_PlateCarrier1_blk";
player addWeapon "hgun_P07_snds_F";
player addWeapon "arifle_MX_Black_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};
 
if(__GETC__(life_coplevel) == 4) then
{
player addUniform "U_Rangemaster";
player addVest "V_PlateCarrier1_blk";
player addWeapon "hgun_P07_snds_F";
player addWeapon "arifle_MX_Black_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};
 
if(__GETC__(life_coplevel) == 5) then
{
player addUniform "U_Rangemaster";
player addVest "V_PlateCarrier1_blk";
player addWeapon "hgun_P07_snds_F";
player addWeapon "arifle_MXC_Black_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};
 
if(__GETC__(life_coplevel) == 6) then
{
player addUniform "U_Rangemaster";
player addVest "V_PlateCarrier1_blk";
player addWeapon "hgun_P07_snds_F";
player addWeapon "arifle_MXM_Black_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};

if(__GETC__(life_coplevel) >= 7) then
{
player addUniform "U_Rangemaster";
player addVest "V_PlateCarrier1_blk";
player addWeapon "hgun_P07_snds_F";
player addWeapon "arifle_MXM_Black_F";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";
};

life_inv_handcuffs = life_inv_handcuffs + 2;
life_inv_handcuffkeys = life_inv_handcuffkeys + 1;
[] call life_fnc_saveGear;