/*
	File: fn_useItem.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main function for item effects and functionality through the player menu.
*/
private["_item"];
disableSerialization;
if((lbCurSel 2005) == -1) exitWith {hint localize "STR_ISTR_SelectItemFirst";};
_item = lbData[2005,(lbCurSel 2005)];

switch (true) do
{
	case (_item == "water" or _item == "coffee"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
		};
	};
	
	case (_item == "gpstracker"): {
		[cursorTarget] spawn life_fnc_gpsTracker;
	};
	
	case (_item == "boltcutter"): {
		[cursorTarget] spawn life_fnc_boltcutter;
		closeDialog 0;
	};
	
	case (_item == "blastingcharge"): {
		player reveal fed_bank;
		(group player) reveal fed_bank;
		[cursorTarget] spawn life_fnc_blastingCharge;
	};
	
	case (_item == "underwatercharge"): {
		player reveal gold_safe;
		(group player) reveal gold_safe;
		[cursorTarget] spawn life_fnc_underwaterCharge;
	};
	
	case (_item == "defusekit"): {
		[cursorTarget] spawn life_fnc_defuseKit;
	};
	
	case (_item == "marijuana"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			ReportLoc = getPos player;
			[[0,format["%1 is chilling with a big Bob Marley special blueberry Blunt!",name player]],"life_fnc_broadcast",civilian,false] spawn life_fnc_MP;
			life_thirst = 50;
			life_hunger = 20;
			[] spawn
			{
				titleText["You are stoned and hungry, chill!","PLAIN"];
				[] spawn life_fnc_weed;
			};
		[[0,format["POLICE REPROT: I have just seen someone smoking WEED in Altis! Is it not YOU'RE JOB TO STOP THIS?! -Anon",name player]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		};
	};
	
	case (_item == "cocainep"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			ReportLoc = getPos player;
			[[0,format["%1 is Snorting a nice line of Nose Candy!!",name player]],"life_fnc_broadcast",civilian,false] spawn life_fnc_MP;
			[] spawn life_fnc_cocain;
			
		[[0,format["POLICE REPROT: I have just seen someone snorting cocain in Altis! Is it not YOU'RE JOB TO STOP THIS?! -Anon",name player]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		};
	};
	
	case (_item == "heroinp"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			ReportLoc = getPos player;
			[[0,format["%1 is Shooting some smack!!!",name player]],"life_fnc_broadcast",civilian,false] spawn life_fnc_MP;
			[] spawn life_fnc_heroin;
			
		[[0,format["POLICE REPROT: I have just seen someone Shooting Heroin in Altis! Is it not YOU'RE JOB TO STOP THIS?! -Anon",name player]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		};
	};
	
	case (_item == "meth"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			ReportLoc = getPos player;
			[[0,format["%1 Scored some meth!",name player]],"life_fnc_broadcast",civilian,false] spawn life_fnc_MP;
			[] spawn life_fnc_meth;
			
		[[0,format["POLICE REPROT: I have just seen someone doing meth in Altis! Is it not YOU'RE JOB TO STOP THIS?! -Anon",name player]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		};
	};	
	
	case (_item == "pinkmeth"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			ReportLoc = getPos player;
			[[0,format["Pink Man is back at it! %1 Scored some of the good meth!",name player]],"life_fnc_broadcast",civilian,false] spawn life_fnc_MP;
			[] spawn life_fnc_meth;
			
		[[0,format["POLICE REPROT: I have just seen someone doing meth in Altis! Is it not YOU'RE JOB TO STOP THIS?! -Anon",name player]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		};
	};
	
	case (_item in ["storagesmall","storagebig","storagehuge"]): {
		[_item] call life_fnc_storageBox;
	};
	
	case (_item == "redgull"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
			[] spawn
			{
				life_redgull_effect = time;
				titleText[localize "STR_ISTR_RedGullEffect","PLAIN"];
				player enableFatigue false;
				waitUntil {!alive player OR ((time - life_redgull_effect) > (3 * 60))};
				player enableFatigue true;
			};
		};
	};
	
	case (_item == "spikeStrip"):
	{
		if(!isNull life_spikestrip) exitWith {hint localize "STR_ISTR_SpikesDeployment"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_spikeStrip;
		};
	};
	
	case (_item == "fuelF"):
	{
		if(vehicle player != player) exitWith {hint localize "STR_ISTR_RefuelInVehicle"};
		[] spawn life_fnc_jerryRefuel;
	};
	/*
	case (_item == "lockpick"):
	{
		[] spawn life_fnc_lockpick;
	};
	*/
	case (_item in ["apple","rabbit","salema","ornate","mackerel","tuna","mullet","catshark","turtle","turtlesoup","donuts","tbacon","peach","burger","hotdog","crayfish"]):
	{
		[_item] call life_fnc_eatFood;
	};

	case (_item == "pickaxe"):
	{
		[] spawn life_fnc_pickAxeUse;
	};
	
	case (_item =="bottledwhiskey"):
	{
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.06;
			if (life_drink < 0.07) exitWith {};
			[] spawn life_fnc_drinkwhiskey;
		};
	};
	
	case (_item =="bottledshine"):
	{
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.08;
			if (life_drink < 0.09) exitWith {};
			[] spawn life_fnc_drinkmoonshine;
		};
	};
	
	case (_item =="bottledbeer"):
	{
		
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.02;
			if (life_drink < 0.06) exitWith {};
			[] spawn life_fnc_drinkbeer;
		};
	};
	
	case (_item == "kidney"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .5;
		};
	};
	
	case (_item == "spleen"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .5;
		};
	};
	
	case (_item == "lung"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .5;
		};
	};
	
	case (_item == "testicles"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .6;
		};
	};
	
	case (_item == "eyes"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .2;
		};
	};	
	
	case (_item == "bodypart"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .2;
		};
	};
	
	case (_item == "leg"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			player setVariable["missingOrgan",false,true];
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue .1;
		};
	};
	
	default
	{
		hint localize "STR_ISTR_NotUsable";
	};
};
	
[] call life_fnc_p_updateMenu;
[] call life_fnc_hudUpdate;