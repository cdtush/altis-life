/*
	File: fn_getfpMission.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Selects a random fp point for a delivery mission.
	Needs to be revised.
*/
private["_fp","_target"];
_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(str(_target) in life_fp_points) then
{
	private["_point"];
	_point = life_fp_points - [(str(_target))];
	_fp = _point call BIS_fnc_selectRandom;
}
	else
{
	_fp = life_fp_points call BIS_fnc_selectRandom;
};

life_fp_start = _target;

life_delivery_in_progress = true;
life_fp_point = call compile format["%1",_fp];

_fp = [_fp,"_"," "] call KRON_Replace;
life_cur_task = player createSimpleTask [format["Delivery_%1",life_fp_point]];
life_cur_task setSimpleTaskDescription [format[localize "STR_NOTF_FPStart",toUpper _fp],"Delivery Job",""];
life_cur_task setTaskState "Assigned";
player setCurrentTask life_cur_task;

["DeliveryAssigned",[format[localize "STR_NOTF_FPTask",toUpper _fp]]] call bis_fnc_showNotification;

[] spawn
{
	waitUntil {!life_delivery_in_progress OR !alive player};
	if(!alive player) then
	{
		life_cur_task setTaskState "Failed";
		player removeSimpleTask life_cur_task;
		["DeliveryFailed",[localize "STR_NOTF_FPFailed"]] call BIS_fnc_showNotification;
		life_delivery_in_progress = false;
		life_fp_point = nil;
	};
};