/*
	File: fn_gather.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main functionality for gathering.
*/
if(isNil "life_action_gathering") then {life_action_gathering = false;};
private["_gather","_itemWeight","_diff","_itemName","_val","_resourceZones","_zone"];
_resourceZones = ["apple_1","apple_2","apple_3","apple_4","peaches_1","peaches_2","peaches_3","peaches_4","peaches_5","water_1","water_2","water_3","water_4","heroin_1","cocaine_1","heroin_2","cocaine_2","weed_2","weed_3","weed_1","scrap","goldc","cfish_1","cfish_2","cfish_3","cfish_4","rye_1","hops_1","corn_1","corn_2","corn_3","wheat_1","wheat_2"];
_zone = "";

if(life_action_gathering) exitWith {}; //Action is in use, exit to prevent spamming.
life_action_gathering = true;

//Find out what zone we're near
{
	if(player distance (getMarkerPos _x) < 30) exitWith {_zone = _x;};
} foreach _resourceZones;

if(_zone == "") exitWith {
	life_action_inUse = false;
};

//Get the resource that will be gathered from the zone name...
switch(true) do {
	case (_zone in ["apple_1","apple_2","apple_3","apple_4"]): {_gather = "apple"; _val = 3;};
	case (_zone in ["peaches_1","peaches_2","peaches_3","peaches_4","peaches_5"]): {_gather = "peach"; _val = 3;};
	case (_zone in ["water_1","water_2","water_3","water_4"]): {_gather = "water"; _val = 3;};
	case (_zone in ["heroin_1","heroin_2"]): {_gather = "heroinu"; _val = 1;};
	case (_zone in ["cocaine_1","cocaine_2"]): {_gather = "cocaine"; _val = 1;};
	case (_zone in ["weed_1","weed_2","weed_3"]): {_gather = "cannabis"; _val = 1;};
	case (_zone in ["scrap"]): {_gather = "iron_r"; _val = 1;};
	case (_zone in ["goldc"]): {_gather = "goldcoin"; _val = 1;};
	case (_zone in ["cfish_1","cfish_2","cfish_3","cfish_4"]): {_gather = "crayfish"; _val = 3;};
	case (_zone in ["rye_1"]): {_gather = "rye"; _val = 2;};
	case (_zone in ["corn_1","corn_2","corn_3"]): {_gather = "corn"; _val = 4;};
	case (_zone in ["hops_1"]): {_gather = "hops"; _val = 2;}; 
	case (_zone in ["wheat_1","wheat_2"]): {_gather = "wheat"; _val = 4;};
	default {""};
};
//gather check??
if(vehicle player != player) exitWith {};

_diff = [_gather,_val,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if(_diff == 0) exitWith {hint localize "STR_NOTF_InvFull"};
life_action_inUse = true;
for "_i" from 0 to 1 do
{
	player playMove "AinvPknlMstpSnonWnonDr_medic4";
	waitUntil{animationState player != "AinvPknlMstpSnonWnonDr_medic4";};
	
};
sleep 4.545;
if(([true,_gather,_diff] call life_fnc_handleInv)) then
{
	_itemName = [([_gather,0] call life_fnc_varHandle)] call life_fnc_varToStr;
	titleText[format[localize "STR_NOTF_Gather_Success",_itemName,_diff],"PLAIN"];
};

life_action_inUse = false;
